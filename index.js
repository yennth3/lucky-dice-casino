const express = require('express');
const app = express();
//khai báo thư viện mongoose
const mongoose = require('mongoose');
const port = 8000;

// Khai báo middleware đọc json
app.use(express.json());

// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

//Import path
const path = require('path');
//import model
const diceHistoryModel = require('./app/models/diceHistoryModel');
const userModel = require('./app/models/userModel');
const prizeHistoryModel = require('./app/models/prizeHistoryModel');
const voucherHistoryModel = require('./app/models/voucherHistoryModel');
app.use(express.static(`views`)); // Use this for show image

//import router
const userRouter = require('./app/routers/userRouter');
const diceHistoryRouter = require('./app/routers/diceHistoryRouter');
const voucherRouter = require('./app/routers/voucherRouter');
const prizeRouter = require('./app/routers/prizeRouter');
const prizeHistoryRouter = require('./app/routers/prizeHistoryRouter');
const voucherHistoryRouter = require('./app/routers/voucherHistoryRouter');
const luckyDiceRouter = require('./app/routers/luckyDiceRouter');

app.get('/', (request, response) => {
  response.sendFile(path.join(`${__dirname}/views/index.html`))
})

//Import middleware
const luckydiceMiddleware = require('./app/middlewares/luckydiceMiddlewares');


app.use('/', luckydiceMiddleware);

//Return random number from 1 to 6 
app.get('/random-number', (req, res) => {
  let randomNum = Math.floor(Math.random() * (6 - 1 + 1)) + 1;
  res.json({
    randomNum: randomNum
  })
})
//kết nối csdl mongodb
mongoose.connect('mongodb://localhost:27017/Lucky_Dice',(error)=>{
    if(error){
        throw error;
    }
    console.log("successfully connected!")
})
//sử dụng rourter
app.use('/', userRouter);
app.use('/', diceHistoryRouter);
app.use('/', voucherRouter);
app.use('/', prizeRouter);
app.use('/', prizeHistoryRouter);
app.use('/', voucherHistoryRouter);
app.use('/', luckyDiceRouter);
app.listen(port, () => {
  console.log(`app listening on port ${port}`);
})