//Import express
const { request, response } = require('express');
const express = require('express');

//Import router
const route = express. Router();

route.use((request, response, next) => {
  console.log(`Time: ${new Date()}`);
  next();
}, (request, response, next) => {
  console.log(`Method: ${request.method}`);
  next();
})

module.exports = route;