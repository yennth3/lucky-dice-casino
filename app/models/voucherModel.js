const mongoose = require ("mongoose");
const Schema = mongoose.Schema;

const voucherSchema = new Schema({
    _id:{
        type: mongoose.Types.ObjectId
    },
    maVoucher:{
        type: String,
        required:true,
        unique:true
    },
    phanTramGiamGia:{
        type:Number,
        required:true
    },
    note:{
        type: String,
        required:false,
    },
    createAt:{
        type:Date,
        default:Date.now()
    },
    updateAt:{
        type:Date,
        default:Date.now()
    }
})
module.exports = mongoose.model("voucher", voucherSchema);