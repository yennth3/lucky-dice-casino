//import course model
const { default: mongoose } = require('mongoose');
const prizeModel = require('../models/prizeModel');

//get all prizes
const getAllPrizes = (request, response) => {
    //b1: thu thập dữ liệu(bỏ qua)
    //b2: kiểm tra dữ liệu (bỏ qua)
    //b3: thực hiện thao tác dữ liệu
    prizeModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        }
        else {
            return response.status(200).json({
                prizes:data
            })
        }
    })
};

//get prize by id
const getPrizeById = (request, response) => {
    //b1: thu thập dữ liệu
    let id = request.params.prizeid;
    //b2: kiểm tra dữ liệu 
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message: "id is invalid!"
        })
    } else {
        //b3: thực hiện thao tác dữ liệu
        prizeModel.findById(id, (error, data) => {
            if (error) {
                return response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            }
            else {
                return response.status(200).json({
                    data
                })
            }
        })
    }
};

//create a Prize
const createPrize = (request, response) => {
    //b1: thu thập dữ liệu
    let body = request.body;
    //b2: kiểm tra dữ liệu
    if (!body.name) {
        return response.status(400).json({
            message: "name is invalid"
        })
    }
 
    //b3: thực hiện
    let prize = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    }
    prizeModel.create(prize, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        } else {
            return response.status(201).json({
                data
            })
        }
    });



};
//update prize
const updatePrize = (request, response) => {
    //b1: thu thập dữ liệu
    let id = request.params.prizeid;
    let body = request.body;
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message: "id is invalid!"
        })
    }
    if (!body.name) {
        return response.status(400).json({
            message: "name is invalid"
        })
    }

    //b3: thực hiện
    let prize = {
        name: body.name,
        description: body.description
    }
    prizeModel.findByIdAndUpdate(id, prize, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        } else {
            return response.status(200).json({
                data
            })
        }
    });

};

//delete prize
const deletePrize = (request, response) => {
    //b1: thu thập
    let id = request.params.prizeid;
    //b2: kiểm tra
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    } else {
        //b3:
        prizeModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                return response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            } else {
                return response.status(204).json({
                    data
                })
            }
        });
    }
};
//export các hàm thành module
module.exports = {
    getAllPrizes: getAllPrizes,
    getPrizeById: getPrizeById,
    createPrize: createPrize,
    updatePrize: updatePrize,
    deletePrize: deletePrize
}
