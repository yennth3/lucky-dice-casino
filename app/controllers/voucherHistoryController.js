//import course model
const { default: mongoose } = require('mongoose');
const voucherHistoryModel = require('../models/voucherHistoryModel');

//create a voucher
const createVoucherHistory = (request, response) => {
    //b1: thu thập dữ liệu
    let body = request.body;
    //b2: kiểm tra dữ liệu
    if (!body.user || !mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            message: "user id is invalid"
        })
    }
    if (!body.voucher || !mongoose.Types.ObjectId.isValid(body.voucher)) {
        return response.status(400).json({
            message: "voucher id is invalid"
        })
    }
    //b3: thực hiện
    let voucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user:body.user,
        voucher: body.voucher
    }
    voucherHistoryModel.create(voucherHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        } else {
            return response.status(201).json({
                data
            })
        }
    });

};

//get all voucher history
const getAllVoucherHistories = (request, response) => {
    //b1: thu thập dữ liệu(bỏ qua)
    let user = request.query.user;
    let condition={};
    if(user){
        condition.user = user;
    }
    console.log(condition);
    //b2: kiểm tra dữ liệu (bỏ qua)
   
    //b3: thực hiện thao tác dữ liệu

    voucherHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        }
        else {
            return response.status(200).json({
                data
            })
        }
    })
};

//get voucher history by id 
const getVoucherHistoryById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let voucherHistoryId = request.params.voucherHistoryId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "voucherHistoryId is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    voucherHistoryModel.findById(voucherHistoryId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get voucher history success",
                data: data
            })
        }
    })
}

const updateVoucherHistoryById  = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let voucherHistoryId = request.params.voucherHistoryId;
    let body = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "voucher History Id is not valid"
        })
    }

    if (!body.user || !mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            message: "user id is invalid"
        })
    }
    
    if (!body.voucher || !mongoose.Types.ObjectId.isValid(body.voucher)) {
        return response.status(400).json({
            message: "voucher id is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let voucherHistoryUpdate = {
        voucher: body.voucher,
        user:body.user
    }

    voucherHistoryModel.findByIdAndUpdate(voucherHistoryId, voucherHistoryUpdate, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update voucher history success",
                data: data
            })
        }
    })
}
const deleteVoucherHistoryById  = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let voucherHistoryId = request.params.voucherHistoryId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "voucher History Id is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    voucherHistoryModel.findByIdAndDelete(voucherHistoryId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete voucher history success"
            })
        }
    })
}

//export các hàm thành module
module.exports = {
    createVoucherHistory: createVoucherHistory,
    getAllVoucherHistories:getAllVoucherHistories,
    getVoucherHistoryById:getVoucherHistoryById,
    updateVoucherHistoryById:updateVoucherHistoryById,
    deleteVoucherHistoryById:deleteVoucherHistoryById
}