//import userModel vào file
const { default: mongoose } = require('mongoose');
const userModel = require('../models/userModel');

const createUser = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let requestBody = request.body;
    //B2: Validate dữ liệu
    if (!requestBody.username) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "username is invalid"
        })
    }
    if (!requestBody.firstname) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "firstname is invalid"
        })
    }
    if (!requestBody.lastname) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "lastname is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let newInput = {
        _id: mongoose.Types.ObjectId(),
        username: requestBody.username,
        firstname: requestBody.firstname,
        lastname: requestBody.lastname
    }

    userModel.create(newInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Create Review Success",
                data: data
            })
        }
    })
}

const getAllUsers = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    userModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get users success",
                data: data
            })
        }
    })
}

const getAllLimitUsers = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let limitRequest = request.query.limit;
    let skipRequest = request.query.skip;
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    userModel.find().sort({username:'asc'}).skip(skipRequest).limit(limitRequest).exec((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get users success",
                data: data
            })
        }
    })
}

const getUserById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "UserId is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user success",
                data: data
            })
        }
    })
}

const updateUserById  = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userid;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User Id is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let userUpdate = {
        username: bodyRequest.username,
        firstname: bodyRequest.firstname,
        lastname: bodyRequest.lastname
    }

    userModel.findByIdAndUpdate(userId, userUpdate, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update user success",
                data: data
            })
        }
    })
}

const deleteUserById  = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userid = request.params.userid;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userid)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User Id is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    userModel.findByIdAndDelete(userid, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete course success"
            })
        }
    })
}


module.exports = {
    createUser:createUser,
    getAllUsers:getAllUsers,
    getUserById:getUserById,
    updateUserById:updateUserById,
    deleteUserById:deleteUserById,
    getAllLimitUsers:getAllLimitUsers
}