//import course model
const { default: mongoose } = require('mongoose');
const userModel = require('../models/userModel');
const voucherModel = require('../models/voucherModel');



//create a Dice
const createLuckyDice = (request, response) => {
    //b1: thu thập dữ liệu
    let body = request.body;
    let randomDice = Math.floor(Math.random() * (6 - 1 + 1)) + 1;
    let userId;
    let voucherRequest;
    let prizeRequest;
    //b2: kiểm tra dữ liệu
    if (!body.firstname) {
        return response.status(400).json({
            message: "firstname is invalid"
        })
    };
    if (!body.lastname) {
        return response.status(400).json({
            message: "lastname is invalid"
        })
    };
    if (!body.username) {
        return response.status(400).json({
            message: "username is invalid"
        })
    }
    //b3: thực hiện
    //get userId
    userModel.find({ username: body.username }, (error, userRequest) => {
        if (error) {
            return response.status(500).json({
                message: error.mesage
            })
        }
        else {
            if (userRequest == "") {
                let newInput = {
                    _id: mongoose.Types.ObjectId(),
                    username: body.username,
                    firstname: body.firstname,
                    lastname: body.lastname
                }
                userModel.create(newInput, (error, data) => {
                    if (error) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: error.message
                        })
                    } else {
                        userId = data._id
                    }
                })
            }
            else {
                userId = userRequest[0]._id;
            }
        }
    })
    //check dice
    console.log(randomDice);
    //get the count off all vouchers
    voucherModel.count().exec(function (err, count){
        //get a random entry
        var random = Math.floor(Math.random()*count);
        //again query all vouchers but only fetch one offset by our random
        voucherModel.findOne().skip(random).exec(
            function(err, result){
                console.log(result);
            }
        )
    })
    if(randomDice<=3){
        voucherRequest = null;
        prizeRequest = null;

    }
    else{
        console.log(voucherRandom);
    }

};

//export các hàm thành module
module.exports = {
    createLuckyDice: createLuckyDice,
}