//import course model
const { default: mongoose } = require('mongoose');
const diceHistoryModel = require('../models/diceHistoryModel');

//create a Dice
const createDiceHistory = (request, response) => {
    //b1: thu thập dữ liệu
    let body = request.body;
    let diceRequest =  Math.floor(Math.random() * (6 - 1 + 1)) + 1;
    //b2: kiểm tra dữ liệu
    if (!body.user || !mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            message: "user id is invalid"
        })
    }
    //b3: thực hiện
    let diceHistory = {
        _id: mongoose.Types.ObjectId(),
        user:body.user,
        dice: diceRequest
    }
    diceHistoryModel.create(diceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        } else {
            return response.status(201).json({
                data
            })
        }
    });

};

//get all dice history
const getAllDiceHistories = (request, response) => {
    //b1: thu thập dữ liệu(bỏ qua)
    let user = request.query.user;
    let condition={};
    if(user){
        condition.user = user;
    }
    console.log(condition);
    //b2: kiểm tra dữ liệu (bỏ qua)
    //b3: thực hiện thao tác dữ liệu
    diceHistoryModel.find(condition,(error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        }
        else {
            return response.status(200).json({
                data
            })
        }
    })
};

//get dice history by id 
const getDiceHistoryById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let diceHistoryId = request.params.diceHistoryId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "diceHistoryId is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    diceHistoryModel.findById(diceHistoryId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get dice history success",
                data: data
            })
        }
    })
}

const updateDiceHistoryById  = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let diceHistoryId = request.params.diceHistoryId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Dice History Id is not valid"
        })
    }
    if (!bodyRequest.dice || Number.isInteger(bodyRequest.dice)==false || bodyRequest.dice<1 || bodyRequest.dice>6) {
        return response.status(400).json({
            message: "dice is invalid"
        })
    }
    if (!bodyRequest.user || !mongoose.Types.ObjectId.isValid(bodyRequest.user)) {
        return response.status(400).json({
            message: "user id is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let diceHistoryUpdate = {
        dice: bodyRequest.dice,
        user:bodyRequest.user
    }

    diceHistoryModel.findByIdAndUpdate(diceHistoryId, diceHistoryUpdate, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update dice history success",
                data: data
            })
        }
    })
}
const deleteDiceHistoryById  = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let diceHistoryId = request.params.diceHistoryId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Dice History Id is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    diceHistoryModel.findByIdAndDelete(diceHistoryId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete course success"
            })
        }
    })
}

//export các hàm thành module
module.exports = {
    createDiceHistory: createDiceHistory,
    getAllDiceHistories:getAllDiceHistories,
    getDiceHistoryById:getDiceHistoryById,
    updateDiceHistoryById:updateDiceHistoryById,
    deleteDiceHistoryById:deleteDiceHistoryById
}