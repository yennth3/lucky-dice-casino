//import course model
const { default: mongoose } = require('mongoose');
const voucherModel = require('../models/voucherModel');

//get all vouchers
const getAllVouchers = (request, response) => {
    //b1: thu thập dữ liệu(bỏ qua)
    //b2: kiểm tra dữ liệu (bỏ qua)
    //b3: thực hiện thao tác dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        }
        else {
            return response.status(200).json({
                vouchers:data
            })
        }
    })
};

//get voucher by id
const getVoucherById = (request, response) => {
    //b1: thu thập dữ liệu
    let id = request.params.voucherid;
    //b2: kiểm tra dữ liệu 
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message: "id is invalid!"
        })
    } else {
        //b3: thực hiện thao tác dữ liệu
        voucherModel.findById(id, (error, data) => {
            if (error) {
                return response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            }
            else {
                return response.status(200).json({
                    data
                })
            }
        })
    }
};

//create a voucher
const createVoucher = (request, response) => {
    //b1: thu thập dữ liệu
    let body = request.body;
    //b2: kiểm tra dữ liệu
    if (!body.code) {
        return response.status(400).json({
            message: "code is invalid"
        })
    }
    if ((!body.discount) || Number.isInteger(body.discount) == false) {
        return response.status(400).json({
            message: "discount is invalid!"
        })
    }

    //b3: thực hiện
    let voucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.code,
        phanTramGiamGia: body.discount,
        note: body.note
    }
    voucherModel.create(voucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        } else {
            return response.status(201).json({
                data
            })
        }
    });



};
//update voucher
const updateVoucher = (request, response) => {
    //b1: thu thập dữ liệu
    let id = request.params.voucherid;
    let body = request.body;
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message: "id is invalid!"
        })
    }
    if (!body.code) {
        return response.status(400).json({
            message: "code is invalid"
        })
    }
    if ((!body.discount) || Number.isInteger(body.discount) == false) {
        return response.status(400).json({
            message: "discount is invalid!"
        })
    }

    //b3: thực hiện
    let voucher = {
        maVoucher: body.code,
        phanTramGiamGia: body.discount,
        note: body.note
    }
    voucherModel.findByIdAndUpdate(id, voucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        } else {
            return response.status(200).json({
                data
            })
        }
    });

};

//delete voucher
const deleteVoucher = (request, response) => {
    //b1: thu thập
    let id = request.params.voucherid;
    //b2: kiểm tra
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    } else {
        //b3:
        voucherModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                return response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            } else {
                return response.status(204).json({
                    data
                })
            }
        });
    }
};
//export các hàm thành module
module.exports = {
    getAllVouchers: getAllVouchers,
    getVoucherById: getVoucherById,
    createVoucher: createVoucher,
    updateVoucher: updateVoucher,
    deleteVoucher: deleteVoucher
}
