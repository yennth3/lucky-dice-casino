//import course model
const { default: mongoose } = require('mongoose');
const prizeHistoryModel = require('../models/prizeHistoryModel');

//create a prize
const createPrizeHistory = (request, response) => {
    //b1: thu thập dữ liệu
    let body = request.body;
    //b2: kiểm tra dữ liệu
    if (!body.user || !mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            message: "user id is invalid"
        })
    }
    if (!body.prize || !mongoose.Types.ObjectId.isValid(body.prize)) {
        return response.status(400).json({
            message: "prize id is invalid"
        })
    }
    //b3: thực hiện
    let prizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user:body.user,
        prize: body.prize
    }
    prizeHistoryModel.create(prizeHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        } else {
            return response.status(201).json({
                data
            })
        }
    });

};

//get all prize history
const getAllPrizeHistories = (request, response) => {
    //b1: thu thập dữ liệu(bỏ qua)
    let user = request.query.user;
    let condition={};
    if(user){
        condition.user = user;
    }
    console.log(condition);
    //b2: kiểm tra dữ liệu (bỏ qua)
    //b3: thực hiện thao tác dữ liệu
    prizeHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        }
        else {
            return response.status(200).json({
                data
            })
        }
    })
};

//get prize history by id 
const getPrizeHistoryById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let prizeHistoryId = request.params.prizeHistoryId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prizeHistoryId is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    prizeHistoryModel.findById(prizeHistoryId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get prize history success",
                data: data
            })
        }
    })
}

const updatePrizeHistoryById  = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let prizeHistoryId = request.params.prizeHistoryId;
    let body = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prize History Id is not valid"
        })
    }

    if (!body.user || !mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            message: "user id is invalid"
        })
    }
    
    if (!body.prize || !mongoose.Types.ObjectId.isValid(body.prize)) {
        return response.status(400).json({
            message: "prize id is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let prizeHistoryUpdate = {
        prize: body.prize,
        user:body.user
    }

    prizeHistoryModel.findByIdAndUpdate(prizeHistoryId, prizeHistoryUpdate, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update prize history success",
                data: data
            })
        }
    })
}
const deletePrizeHistoryById  = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let prizeHistoryId = request.params.prizeHistoryId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prize History Id is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    prizeHistoryModel.findByIdAndDelete(prizeHistoryId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete prize history success"
            })
        }
    })
}

//export các hàm thành module
module.exports = {
    createPrizeHistory: createPrizeHistory,
    getAllPrizeHistories:getAllPrizeHistories,
    getPrizeHistoryById:getPrizeHistoryById,
    updatePrizeHistoryById:updatePrizeHistoryById,
    deletePrizeHistoryById:deletePrizeHistoryById
}