//khai báo thư viện 
const express = require("express");

//import module voucher controller
const {getAllVouchers, getVoucherById, createVoucher, updateVoucher, deleteVoucher} = require('../controllers/voucherController');

//tạo router
const voucherRouter = express.Router();

//get all voucher
voucherRouter.get("/vouchers",getAllVouchers)

//get voucher
voucherRouter.get("/vouchers/:voucherid",getVoucherById)

//create a voucher
voucherRouter.post("/vouchers",createVoucher)

//update voucher
voucherRouter.put("/vouchers/:voucherid",updateVoucher)

//delete voucher
voucherRouter.delete("/vouchers/:voucherid", deleteVoucher)

module.exports = voucherRouter;