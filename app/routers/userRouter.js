//khai báo thư viện 
const express = require("express");

const userRouter = express.Router();
const {getAllLimitUsers, createUser, getAllUsers, getUserById, updateUserById, deleteUserById} = require('../controllers/userController');
//get All users
userRouter.get("/users", getAllUsers)
userRouter.get("/users-limit", getAllLimitUsers)
//create a user
userRouter.post("/users", createUser);

//get user by id
userRouter.get("/users/:userId", getUserById);

//update users
userRouter.put("/users/:userid", updateUserById)

//delete user
userRouter.delete("/users/:userid", deleteUserById)

module.exports = userRouter;