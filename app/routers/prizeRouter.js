//khai báo thư viện 
const express = require("express");

//import module prize controller
const {getAllPrizes, getPrizeById, createPrize, updatePrize, deletePrize} = require('../controllers/prizeController');

//tạo router
const prizeRouter = express.Router();

//get all prize
prizeRouter.get("/prizes",getAllPrizes)

//get prize
prizeRouter.get("/prizes/:prizeid",getPrizeById)

//create a prize
prizeRouter.post("/prizes",createPrize)

//update prize
prizeRouter.put("/prizes/:prizeid",updatePrize)

//delete prize
prizeRouter.delete("/prizes/:prizeid", deletePrize)

module.exports = prizeRouter;