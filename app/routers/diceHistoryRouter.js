//khai báo thư viện 
const express = require("express");

//import module dice controller
const {createDiceHistory, getAllDiceHistories, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById} = require('../controllers/diceHistoryController');

//tạo router
const diceHistoryRouter = express.Router();

//create a history
diceHistoryRouter.post("/dice-histories",createDiceHistory);

//get all dice histories
diceHistoryRouter.get("/dice-histories",getAllDiceHistories);

//get dice histories by id
diceHistoryRouter.get("/dice-histories/:diceHistoryId",getDiceHistoryById)

//update dice histories by id
diceHistoryRouter.put("/dice-histories/:diceHistoryId",updateDiceHistoryById)

//delete dice histories by id
diceHistoryRouter.delete("/dice-histories/:diceHistoryId",deleteDiceHistoryById)

module.exports = diceHistoryRouter;