//khai báo thư viện 
const express = require("express");

//import module dice controller
const {diceHandler, getDiceHistoryOfUsername, getPrizeHistoryOfUsername, getVoucherHistoryOfUsername} = require('../controllers/diceController');

//tạo router
const diceRouter = express.Router();

//create a dice
diceRouter.post("/devcamp-lucky-dice/dice",diceHandler);

//get dice history of username
diceRouter.get("/devcamp-lucky-dice/dice-history",getDiceHistoryOfUsername);

//get prize history of username
diceRouter.get("/devcamp-lucky-dice/prize-history",getPrizeHistoryOfUsername);

//get voucher history of username
diceRouter.get("/devcamp-lucky-dice/voucher-history",getVoucherHistoryOfUsername);
module.exports = diceRouter;