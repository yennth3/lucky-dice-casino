//khai báo thư viện 
const express = require("express");

//import module voucher controller
const {createVoucherHistory, getAllVoucherHistories, getVoucherHistoryById, updateVoucherHistoryById, deleteVoucherHistoryById} = require('../controllers/voucherHistoryController');

//tạo router
const voucherHistoryRouter = express.Router();

//create a history
voucherHistoryRouter.post("/voucher-histories",createVoucherHistory);

//get all voucher histories
voucherHistoryRouter.get("/voucher-histories",getAllVoucherHistories);

//get voucher histories by id
voucherHistoryRouter.get("/voucher-histories/:voucherHistoryId",getVoucherHistoryById)

//update voucher histories by id
voucherHistoryRouter.put("/voucher-histories/:voucherHistoryId",updateVoucherHistoryById)

//delete voucher histories by id
voucherHistoryRouter.delete("/voucher-histories/:voucherHistoryId",deleteVoucherHistoryById)

module.exports = voucherHistoryRouter;