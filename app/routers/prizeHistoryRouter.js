//khai báo thư viện 
const express = require("express");

//import module prize controller
const {createPrizeHistory, getAllPrizeHistories, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById} = require('../controllers/prizeHistoryController');

//tạo router
const prizeHistoryRouter = express.Router();

//create a history
prizeHistoryRouter.post("/prize-histories",createPrizeHistory);

//get all Prize histories
prizeHistoryRouter.get("/prize-histories",getAllPrizeHistories);

//get prize histories by id
prizeHistoryRouter.get("/prize-histories/:prizeHistoryId",getPrizeHistoryById)

//update prize histories by id
prizeHistoryRouter.put("/prize-histories/:prizeHistoryId",updatePrizeHistoryById)

//delete prize histories by id
prizeHistoryRouter.delete("/prize-histories/:prizeHistoryId",deletePrizeHistoryById)

module.exports = prizeHistoryRouter;